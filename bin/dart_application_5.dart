import 'dart:io';

class Taobinmachine {
  showWelcome() {
    print("Welcome to Taobin coffee vending machine");
  }

  showMenu() {
    List<String> menu = [
      "Green tea",
      "Thai tea",
      "Latte",
      "Espresso",
      "Milk tea"
    ];
    List<int> price = [45, 45, 50, 55, 40];
    int j = 1;
    for (int i = 0; i < menu.length; i++) {
      print("$j ${menu[i]} ${price[i]} Baht");
      j++;
    }
    print("Please input number to select");
    int select = int.parse(stdin.readLineSync()!);
    int pay = price[select];
    return pay;
  }

  calculate() {
    int pay = showMenu();
    print("You need to pay $pay Baht");
    print("Please input money");
    int coin = int.parse(stdin.readLineSync()!);
    print("this is change ${coin - pay} Baht");
  }
}

class Member {
  int point = 0;
  var phonenumber;
  inputphone() {
    print("Please input member number");
    phonenumber = stdin.readLineSync();
    point++;
    print("Member $phonenumber  point is $point");
  }
}

void main() {
  Taobinmachine taobin = new Taobinmachine();
  taobin.showWelcome();
  taobin.calculate();
  Member member = new Member();
  member.inputphone();
}
